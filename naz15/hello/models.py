from django.db import models
import time


def merge(spisok):
    if len(spisok) > 1:
        mid = len(spisok) // 2
        lefthalf = spisok[:mid]
        righthalf = spisok[mid:]

        merge(lefthalf)
        merge(righthalf)

        i = 0
        j = 0
        k = 0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                spisok[k] = lefthalf[i]
                i = i + 1
            else:
                spisok[k] = righthalf[j]
                j = j + 1
            k = k + 1

        while i < len(lefthalf):
            spisok[k] = lefthalf[i]
            i = i + 1
            k = k + 1

        while j < len(righthalf):
            spisok[k] = righthalf[j]
            j = j + 1
            k = k + 1
    return spisok


def sort_time(function):
    def wrapped(*args):
        start = time.clock()
        result = function(*args)
        vremya = time.clock() - start
        return [result, '{:.10f}'.format(vremya)]
    return wrapped


class Base(models.Model):
    class Meta:
        abstract = True


class Bubble(Base):
    bubble_id = models.AutoField(primary_key=True)
    @sort_time
    def bubble_sort(self):
        chisla = self.spisok
        spisok = list(map(int, chisla.split(',')))
        for i in range(len(spisok) - 1):
            for k in range(len(spisok) - 1 - i):
                if spisok[k] > spisok[k + 1]:
                    spisok[k], spisok[k + 1] = spisok[k + 1], spisok[k]
        result = ','.join(map(str, spisok))
        return result


class Insertion(Base):
    insertion_id = models.AutoField(primary_key=True)
    @sort_time
    def insertion_sort(self):
        chisla = self.spisok
        spisok = list(map(int, chisla.split(',')))
        for i in range(1, len(spisok)):
            arg = spisok[i]
            j = i - 1
            while arg < spisok[j]:
                spisok[j + 1] = spisok[j]
                j = j - 1
                spisok[j + 1] = arg
        result = ','.join(map(str, spisok))
        return result



class Merge(Base):
    merge_id = models.AutoField(primary_key=True)
    @sort_time
    def merge_sort(self):
        chisla = self.spisok
        spisok = list(map(int, chisla.split(',')))
        pre_result = merge(spisok)
        result = ','.join(map(str, pre_result))
        return result


class Main(Merge, Bubble, Insertion):
    sort_name = models.CharField(max_length=20, default=None)
    spisok = models.TextField(blank=True, null=True, default=None)
    sort_spisok = models.TextField(blank=True, null=True, default=None)
    time_for_sorting = models.TextField(blank=True, null=True, default=None)

    def __str__(self):
        return f"{self.sort_name}"

    def save(self, *args, **kwargs):
        if self.sort_name == 'Bubble':
            result = self.bubble_sort()
        if self.sort_name == 'Merge':
            result = self.merge_sort()
        if self.sort_name == 'Insertion':
            result = self.insertion_sort()
        self.sort_spisok = result[0]
        self.time_for_sorting = result[1]
        super(Main, self).save(*args, **kwargs)