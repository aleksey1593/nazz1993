from django.shortcuts import render
from .models import Main


def hello(request):
    return render(request, 'index.html', locals())


def send(request):
    if request.POST and request.FILES:
        file = request.FILES['file']
        text = str(file.read())
        start = text.find('\'')
        finish = text.find('\'', start + 1)
        spisok = text[start+1:finish-2]
        for key in request.POST:
            if not key == 'csrfmiddlewaretoken':
                method = request.POST[key]
        Main.objects.create(sort_name=method, spisok=spisok)
        return render(request, 'thanks.html')
    return render(request, 'error.html')

