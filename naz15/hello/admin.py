from django.contrib import admin
from .models import Main


class Mainadmin(admin.ModelAdmin):
    list_display = ['sort_name', 'spisok', 'sort_spisok', 'time_for_sorting']
    list_filter = ['sort_name']
    search_fields = ['sort_name', 'spisok', 'sort_spisok', 'time_for_sorting']
    ordering = ['sort_name', 'spisok', 'sort_spisok', 'time_for_sorting']

    class Meta:
        model=Main

admin.site.register(Main, Mainadmin)
